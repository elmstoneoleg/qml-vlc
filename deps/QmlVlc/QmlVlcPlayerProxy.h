/*******************************************************************************
* Copyright © 2014-2015, Sergey Radionov <rsatom_gmail.com>
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*   1. Redistributions of source code must retain the above copyright notice,
*      this list of conditions and the following disclaimer.
*   2. Redistributions in binary form must reproduce the above copyright notice,
*      this list of conditions and the following disclaimer in the documentation
*      and/or other materials provided with the distribution.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
* THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
* BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY,
* OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
* PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
* OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
* WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
* OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*******************************************************************************/

#pragma once

#include <QObject>
#include <QTimer>
#include <QEvent>
#include <QQmlParserStatus>

#include "QmlVlcVideoSource.h"
#include "QmlVlcAudio.h"
#include "QmlVlcInput.h"
#include "QmlVlcPlaylist.h"
#include "QmlVlcSubtitle.h"
#include "QmlVlcVideo.h"
#include "QmlVlcMedia.h"

class QmlVlcPlayerProxy
        : public QmlVlcVideoSource,
        protected vlc::media_player_events_callback
{
        Q_OBJECT

    public:

        explicit QmlVlcPlayerProxy( const std::shared_ptr<vlc::playlist_player_core>& player,
                                    QObject* parent = nullptr );

        ~QmlVlcPlayerProxy() override;

        enum State {
            Opening = ::libvlc_Opening,
            Buffering = ::libvlc_Buffering,
            Playing = ::libvlc_Playing,
            Paused = ::libvlc_Paused,
            Stopped = ::libvlc_Stopped,
            Ended = ::libvlc_Ended,
            Error = ::libvlc_Error,
        };
        Q_ENUMS( State )

        Q_PROPERTY( QString vlcVersion READ vlcVersion )
        Q_PROPERTY( bool playing READ playing NOTIFY playingChanged )
        Q_PROPERTY( qint64 length READ length NOTIFY lengthChanged )
        Q_PROPERTY( State state READ state NOTIFY stateChanged )
        Q_PROPERTY( float position READ position WRITE setPosition NOTIFY positionChanged )
        Q_PROPERTY( double time READ time WRITE setTime NOTIFY timeChanged )
        Q_PROPERTY( unsigned volume READ volume WRITE setVolume NOTIFY volumeChanged )
        Q_PROPERTY( qint64 duration READ duration NOTIFY durationChanged )
        Q_PROPERTY( QSize nativeResolution READ nativeResolution NOTIFY nativeResolutionChanged )
        Q_PROPERTY( QmlVlcInput* input READ input CONSTANT )
        Q_PROPERTY( QmlVlcVideo* video READ video CONSTANT )
        Q_PROPERTY( QmlVlcAudio* audio READ audio CONSTANT )
        Q_PROPERTY( QmlVlcSubtitle* subtitle READ subtitle CONSTANT )
        Q_PROPERTY( QmlVlcPlaylist* playlist READ playlist CONSTANT )
        Q_PROPERTY( QmlVlcMedia* mediaDescription READ mediaDescription CONSTANT )

    public:

        Q_INVOKABLE bool setSource(MediaType type, const QString & source) override;
        Q_INVOKABLE qint64 duration() const override;
        Q_INVOKABLE void loadMedia() override;
        Q_INVOKABLE void unloadMedia() override;
        Q_INVOKABLE float position() const override;
        Q_INVOKABLE void mute() override;
        Q_INVOKABLE void unMute() override;
        Q_INVOKABLE QSize nativeResolution() const override;
        Q_INVOKABLE void togglePause() override;
        Q_INVOKABLE QString vlcVersion() override;
        Q_INVOKABLE void play() override;
        Q_INVOKABLE void toggleMute() override;
        Q_INVOKABLE bool playing() override;
        Q_INVOKABLE qint64 length() override;
        Q_INVOKABLE double time() override;
        Q_INVOKABLE void setTime(double time) override;
        Q_INVOKABLE void setVolume(unsigned int volume) override;
        Q_INVOKABLE unsigned int volume() const override;
        Q_INVOKABLE virtual void setLooping(bool repeat) override;

        State state() const;

        vlc::playlist_player_core& player() const
        { assert( m_player ); return *m_player; }

        const std::shared_ptr<vlc::playlist_player_core>& player_ptr()
        { return m_player; }

    Q_SIGNALS:

        /* async events from libvlc */
        void mediaChanged();
        void titleChanged();
        void timeChanged(double time);
        void positionChanged(float position);
        void seekableChanged(bool seekable);
        void pausableChanged(bool pausable);
        void lengthChanged(double length);
        void volumeChanged();
        void playingChanged();
        void stateChanged(QmlVlcPlayerProxy::State state);
        void durationChanged(qint64 position);
        void nativeResolutionChanged(const QSize & size);

    public Q_SLOTS:

        Q_INVOKABLE void start() override;
        Q_INVOKABLE void stop() override;
        Q_INVOKABLE void pause() override;
        Q_INVOKABLE void resume() override;
        Q_INVOKABLE void setPosition(float position) override;
        Q_INVOKABLE void startSeek() override;
        Q_INVOKABLE void endSeek() override;

    protected:

        struct LibvlcEvent;
        virtual void handleLibvlcEvent( const LibvlcEvent& );
        virtual void classBegin() override;
        virtual void componentComplete() override;
        void media_player_event( const libvlc_event_t* e ) override;
        bool event( QEvent* ) override;

        void classEnd();

    private Q_SLOTS:

        void nativeSizeChanged(const QSize& size) override;
        void currentItemEndReached();

    public:

        QmlVlcAudio*     audio()     { return &m_audio; }
        QmlVlcInput*     input()     { return &m_input; }
        QmlVlcPlaylist*  playlist()  { return &m_playlist; }
        QmlVlcSubtitle*  subtitle()  { return &m_subtitle; }
        QmlVlcVideo*     video()     { return &m_video; }
        QmlVlcMedia*     mediaDescription() { return &m_currentMediaDesc; }

    private:

        std::shared_ptr<vlc::playlist_player_core>     m_player;
        QString                                        m_mrl;
        unsigned int                                   m_MediaItem = 0;
        QmlVlcAudio                                    m_audio;
        QmlVlcInput                                    m_input;
        QmlVlcPlaylist                                 m_playlist;
        QmlVlcSubtitle                                 m_subtitle;
        QmlVlcVideo                                    m_video;
        QmlVlcCurrentMedia                             m_currentMediaDesc;
        QTimer                                         m_errorTimer;
};

struct QmlVlcPlayerProxy::LibvlcEvent : public QEvent
{
        enum {
            LibvlcEventId = QEvent::User,
        };

        LibvlcEvent( const libvlc_event_t& );

        libvlc_event_t _libvlcEvent;
};
