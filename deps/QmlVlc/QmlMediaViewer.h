#ifndef MEDIAVIEWER_H
#define MEDIAVIEWER_H

#include <QObject>

class MediaViewer : public QObject
{
        Q_OBJECT

    public:

        MediaViewer(QObject* parent):
            QObject(parent) {}

        enum MediaType {
            MediaTypeNone,
            MediaTypeImage,
            MediaTypeVideo,
            MediaTypeAudio,
            MediaTypeCanvas,
            MediaTypeFile,
            MediaTypePlugin
        };
        Q_ENUMS(MediaType)

        virtual bool             setSource(MediaType type, const QString & source) = 0;
        virtual qint64           duration() const = 0;
        virtual float            position() const = 0;
        virtual QSize            nativeResolution() const = 0; // what is the native resolution of the media?
        virtual void             loadMedia() = 0; // load and unload, we dont want to consume CPU and memory for items on pages not visible
        virtual void             unloadMedia() = 0;
        virtual QString          vlcVersion()= 0;
        virtual void             mute() = 0;
        virtual void             unMute() = 0;
        virtual void             play() = 0;
        virtual bool             playing() = 0;
        virtual void             toggleMute() = 0;
        virtual qint64           length() = 0;
        virtual double           time() = 0;
        virtual void             setTime(double time) = 0;
        virtual unsigned int     volume() const = 0;
        virtual void             setVolume(unsigned int volume) = 0;
        virtual void             togglePause() = 0;
        virtual void             setLooping(bool repeat) = 0;

    Q_SIGNALS:

        void                     started();
        void                     stopped();
        void                     paused();
        void                     resumed();
        void                     opening();
        void                     buffering(float percents);
        void                     forward();
        void                     backward();
        void                     endReached();
        void                     error(const QString & str = QString()); // This should be fired when there is an unrecoverable failure

    public Q_SLOTS: // is it necessary implement as slots?

        virtual void             start() = 0;
        virtual void             stop() = 0;
        virtual void             pause() = 0;
        virtual void             resume() = 0;
        virtual void             setPosition(float position) = 0;
        virtual void             startSeek() = 0; // no implementation yet, need requirements what to implement inside the slot
        virtual void             endSeek() = 0; // no implementation yet, need requirements what to implement inside the slot
};

#endif // MEDIAVIEWER_H
