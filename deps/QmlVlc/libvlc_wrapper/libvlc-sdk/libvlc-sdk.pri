INCLUDEPATH += $$PWD/include

VLCLIBDIR = $$PWD/lib/msvc
VLCRESDIR32 = vlcResWin32
VLCRESDIR64 = vlcResWin64
VLCLIB32 = libvlc.lib
VLCLIB64 = libvlc.x64.lib
VLCDESTDIR = $$clean_path($$OUT_PWD)
DESTDIR = $$VLCDESTDIR
VLCDESTDIR = $$shell_quote($$VLCDESTDIR)

VLCRAWMACLIBDIR = $$PWD/lib/mac/lib
VLCMACLIBDIR = $$shell_quote($$clean_path($$PWD/lib/mac/lib))
VLCMACPLUGINDIR = $$shell_quote($$clean_path($$PWD/lib/mac/plugins))
VLCMACSHAREDIR =  $$shell_quote($$clean_path($$PWD/lib/mac/share))
MAC_APP_EXT = .app
MAC_BUNDLE_RES_DEST = Contents/MacOS

win32 {
contains(QMAKE_TARGET.arch, x86_64) {
             LIBS += $$PWD/lib/msvc/$$VLCLIB64
             VLCRESDIR = $$shell_quote($$clean_path($$VLCLIBDIR/$$VLCRESDIR64))
        }
        else {
             LIBS += $$PWD/lib/msvc/$$VLCLIB32
             VLCRESDIR = $$shell_quote($$clean_path($$VLCLIBDIR/$$VLCRESDIR32))
        }

    VLCRESDIR = $$system_path($$VLCRESDIR)
    VLCDESTDIR = $$system_path($$VLCDESTDIR)

    resourcedata.commands = $(COPY_DIR) $$VLCRESDIR $$VLCDESTDIR
    first.depends = $(first) resourcedata
    export(first.depends)
    export(resourcedata.commands)
    QMAKE_EXTRA_TARGETS += first resourcedata

} else: !android {
    LIBS += -lvlc
}

macx {
    QMAKE_TARGET_BUNDLE_PREFIX = qml-vlc-demo

    LIBS += -L/$$VLCRAWMACLIBDIR

    app_bundle {
        VLCMACLIBDIRDEST = $$shell_quote($$clean_path($$TARGET$$MAC_APP_EXT/$$MAC_BUNDLE_RES_DEST))

        QMAKE_POST_LINK += cp -R $$VLCMACLIBDIR $$VLCMACLIBDIRDEST $$escape_expand(\n\t)
        QMAKE_POST_LINK += cp -R $$VLCMACPLUGINDIR $$VLCMACLIBDIRDEST $$escape_expand(\n\t)
        QMAKE_POST_LINK += cp -R $$VLCMACSHAREDIR $$VLCMACLIBDIRDEST $$escape_expand(\n\t)
    }
}
