import QtQuick 2.1
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 1.0
import QmlVlc 0.1
import QtMultimedia 5.0

Rectangle {
    id: root
    width: 800
    height: 600
    color: 'lightgrey';

    readonly property int margin: 20

    VlcPlayer {
        id: vlcPlayer;
    }

    VlcVideoSurface {
        source: vlcPlayer;
        x: margin
        y: margin
        width: root.width  - root.margin * 2;
        height: root.height - root.margin;
    }

    FileDialog {
        id: fileDialog
        title: "Please choose a video file"
        folder: shortcuts.movies

        onAccepted: {
            urlBox.text = fileDialog.fileUrls[0]
            vlcPlayer.setSource(MediaViewer.MediaTypeVideo, urlBox.text)
            vlcPlayer.start()
        }

        onRejected: console.log("Canceled")
    }

    MouseArea {
        hoverEnabled: true
        anchors.fill: parent

        RowLayout {
            id: openSources
            x: root.margin
            y: root.margin + 4

            TextField
            {
                id: urlBox
                placeholderText: qsTr("Enter URL here and click \"Open URL\" button")
                implicitWidth: root.width - (openVideoURLButton.width * 2 + root.margin * 2)

                background: Rectangle {
                    radius: 4
                }
            }

            Button {
                id: openVideoURLButton
                implicitHeight: urlBox.implicitHeight
                text: qsTr("Open URL")
                onClicked: {
                    if(urlBox.text != "")
                    {
                        vlcPlayer.setSource(MediaViewer.MediaTypeVideo, urlBox.text)
                        vlcPlayer.start()
                    }
                    else
                        console.log("URL is empty")
                }
            }

            Button {
                id: openVideoFileButton
                implicitHeight: urlBox.implicitHeight
                text: qsTr("Open file")

                onClicked: fileDialog.visible = true
            }
        }

        Text
        {
            id: versionText
            x: root.margin / 4
            y: 3
            font.pixelSize: 12
            text: qsTr("VLC version: ") + vlcPlayer.vlcVersion
            color: "grey"
        }

        RowLayout {
            id: toolbar
            opacity: .55
            spacing: 8
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.containsMouse ? 0 : -height
            anchors.leftMargin: spacing
            anchors.rightMargin: spacing / 2

            Behavior on anchors.bottomMargin { PropertyAnimation { duration: 250} }

            CheckBox
            {
                id: loopCheckBox
                checked: false
                text: qsTr("loop")
                onCheckedChanged: vlcPlayer.setLooping(loopCheckBox.checked)
            }

            Rectangle {
                height: 24
                width: 24
                radius: width * 0.25
                color: 'black'
                border.width: 1
                border.color: 'white'

                Image {
                    source: vlcPlayer.playing ? "pause.png" : "play.png"
                    anchors.centerIn: parent
                }
                MouseArea {
                    anchors.fill: parent
                    onClicked: vlcPlayer.togglePause()
                }
            }
            Rectangle {
                id: videoProgressBar
                Layout.fillWidth: true
                Layout.rightMargin: 15
                height: 10
                color: 'transparent'
                border.width: 1
                border.color: 'white'
                anchors.verticalCenter: parent.verticalCenter

                MouseArea {
                    anchors.fill: parent
                    onClicked: vlcPlayer.setTime(mouseX * (vlcPlayer.duration / videoProgressBar.width))
                }

                Rectangle {
                    id: progressIndicator
                    width: (parent.width - anchors.leftMargin - anchors.rightMargin) * vlcPlayer.position
                    color: 'white'
                    anchors.margins: 2
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.bottom: parent.bottom
                }
            }
        }
    }
}
